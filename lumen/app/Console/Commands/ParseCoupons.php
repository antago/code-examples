<?php

namespace App\Console\Commands;

use App\Contracts\CouponParserServiceInterface;
use App\Repositories\CouponRepository;
use App\Repositories\SourceRepository;
use App\Source;
use Exception;
use Illuminate\Console\Command;

/**
 * Class ParseCoupons
 * @package App\Console\Commands
 */
class ParseCoupons extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'coupons:parse {source?} {--no-proxy}';

    /**
     * @var CouponRepository
     */
    private CouponRepository $couponRepository;

    /**
     * @var SourceRepository
     */
    private SourceRepository $sourceRepository;

    /**
     * @var string|null
     */
    private ?string $sourceAlias;

    /**
     * @var bool
     */
    private bool $useProxy;

    /**
     * ParseCoupons constructor.
     *
     * @param CouponRepository $couponRepository
     * @param SourceRepository $sourceRepository
     */
    public function __construct
    (
        CouponRepository $couponRepository,
        SourceRepository $sourceRepository
    )
    {
        parent::__construct();
        $this->couponRepository = $couponRepository;
        $this->sourceRepository = $sourceRepository;
    }

    /**
     * @throws Exception
     */
    public function handle(): void
    {
        $this->sourceAlias = $this->argument('source');
        $this->useProxy = !$this->option('no-proxy');

        if ($this->sourceAlias) {
            $source = $this->sourceRepository->getByAlias($this->sourceAlias);

            if (!$source) {
                echo 'Source with alias ' . $this->sourceAlias . ' not found';
                return;
            }

            $this->parseSource($source);
        } else {
            $this->parseAllSources();
        }
    }

    /**
     * @return void
     */
    private function parseAllSources(): void
    {
        $sources = Source::all();

        foreach ($sources as $source) {
            try {
                $this->parseSource($source);
            } catch (Exception $exception) {
                continue;
            }
        }
    }

    /**
     * @param Source $source
     *
     * @return void
     * @throws Exception
     */
    private function parseSource(Source $source): void
    {
        $className = 'App\\Services\\CouponParser\\' . $source->alias . 'ParserService';
        /** @var CouponParserServiceInterface $service */
        $service = app($className);
        $service->setUseProxy($this->useProxy);
        $service->fetchSource();
    }
}
