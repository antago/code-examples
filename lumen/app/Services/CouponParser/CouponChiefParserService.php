<?php

namespace App\Services\CouponParser;

use App\Exceptions\ParserException;
use Carbon\Carbon;
use Exception;
use Symfony\Component\DomCrawler\Crawler as DomParser;
use Throwable;

/**
 * Class CouponChiefParserService
 * @package App\Services
 */
class CouponChiefParserService extends CouponParserService
{
    /**
     * seconds
     */
    const MIN_REQUEST_TIMEOUT = 60;
    const MAX_REQUEST_TIMEOUT = 120;

    /***
     * @return void
     */
    protected function fetchSourcePage(): void
    {
        $this->couponData->getPageNode()->filter('.store-coupon')->each(function ($couponNode) {
            $this->couponData->setCouponNode($couponNode);
            $this->fetchCoupon();
        });
    }

    /***
     * @return string
     * @throws ParserException
     */
    protected function parseCouponTitle(): string
    {
        $title = $this->couponData->getCouponNode()->filter('.discount-text-link')->text();

        if (!$title) {
            throw new ParserException($this->source, 'Error getting coupon title');
        }

        return $title;
    }

    /***
     * @return string
     * @throws ParserException
     */
    protected function parseCouponLink(): string
    {
        try {
            $initialLink = $this->couponData->getCouponNode()->filter('.discount-text-link')->link()->getUri();
            $link = $this->getEndLink($initialLink);
        } catch (Throwable $exception) {
            throw new ParserException($this->source, 'Error getting coupon link');
        }

        return $link;
    }

    /**
     * @return string|null
     * @throws ParserException
     * @throws Throwable
     */
    protected function parseCouponCode(): ?string
    {
        $code = null;

        try {
            if ($this->couponHasCode()) {
                $codeHtml = $this->getCouponCodeHtml();
                $code = $codeHtml->filter('#coupon-code')->attr('value');
            }
        } catch (Exception $exception) {
            throw new ParserException($this->source, 'Error getting coupon code');
        }

        return $code;
    }

    /***
     * @return bool
     */
    protected function couponHasCode(): bool
    {
        return strpos($this->couponData->getCouponNode()->attr('class'), 'store-code') !== false;
    }

    /**
     * @return DomParser
     * @throws Throwable
     */
    protected function getCouponCodeHtml(): DomParser
    {
        $content = $this->getContent($this->source->baseUrl . '/pages/coupon_overlay/' . $this->couponData->getExternalId());

        return new DomParser($content);
    }

    /***
     * @return Carbon|null
     */
    protected function parseCouponExpires(): ?Carbon
    {
        try {
            $dateTime = $this->couponData->getCouponNode()->filter('time')->attr('datetime');
        } catch (Exception $e) {
            return null;
        }

        return $dateTime ? Carbon::parse($dateTime) : null;
    }

    /**
     *
     * @return string
     * @throws ParserException
     */
    protected function parseCouponExternalId(): string
    {
        $externalId = $this->couponData->getExternalId() ?? (int)$this->couponData->getCouponNode()->attr('data-coupon-id');

        if (!$externalId) {
            throw new ParserException($this->source, 'Error getting external id');
        }

        return $externalId;
    }

    /**
     * @param string $response
     *
     * @return bool
     * @throws ParserException
     */
    protected function checkContent(string $response): bool
    {
        if (strpos($response, 'distil_r_blocked.html') !== false) {
            throw new ParserException($this->source, 'We were blocked');
        } elseif (strpos($response, 'distil_r_captcha.html') !== false) {
            throw new ParserException($this->source, 'Captcha was given to us');
        }

        return true;
    }
}
