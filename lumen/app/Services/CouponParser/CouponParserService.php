<?php

namespace App\Services\CouponParser;

use App\Contracts\CouponParserServiceInterface;
use App\{Coupon, Proxy, Source};
use App\DataObjects\CouponData;
use App\Exceptions\ParserException;
use App\Repositories\{CouponRepository, ProxyRepository, SourceRepository};
use App\Services\HttpClientService;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Log;
use Symfony\Component\DomCrawler\Crawler as DomParser;
use Symfony\Contracts\HttpClient\ResponseInterface;
use Throwable;

/**
 * Class CouponParserService
 * @package App\Services\CouponParser
 */
abstract class CouponParserService implements CouponParserServiceInterface
{
    /**
     * seconds
     */
    const MIN_REQUEST_TIMEOUT = 2;
    const MAX_REQUEST_TIMEOUT = 3;

    const HEADERS = [
        'accept' => '*/*',
        'user-agent' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36',
    ];

    /**
     * @var Source
     */
    protected Source $source;

    /**
     * @var HttpClientService
     */
    protected HttpClientService $httpClientService;

    /**
     * @var ProxyRepository
     */
    protected ProxyRepository $proxyRepository;

    /**
     * @var DomParser
     */
    protected DomParser $domParser;

    /**
     * @var CouponRepository
     */
    protected CouponRepository $couponRepository;

    /**
     * @var SourceRepository
     */
    protected SourceRepository $sourceRepository;

    /**
     * @var bool
     */
    protected bool $useProxy = true;

    /**
     * @var CouponData
     */
    protected CouponData $couponData;

    /**
     * @throws ParserException
     */
    abstract protected function fetchSourcePage(): void;

    /**
     * @return string
     * @throws ParserException
     */
    abstract protected function parseCouponTitle(): string;

    /**
     * @return string
     * @throws ParserException
     */
    abstract protected function parseCouponLink(): string;

    /**
     * @return string|null
     * @throws ParserException
     */
    abstract protected function parseCouponCode(): ?string;

    /**
     * CouponParserService constructor.
     *
     * @param HttpClientService $httpClientService
     * @param ProxyRepository $proxyRepository
     * @param DomParser $domParser
     * @param CouponRepository $couponRepository
     * @param SourceRepository $sourceRepository
     *
     * @throws Exception
     */
    public function __construct
    (
        HttpClientService $httpClientService,
        ProxyRepository $proxyRepository,
        DomParser $domParser,
        CouponRepository $couponRepository,
        SourceRepository $sourceRepository
    )
    {
        $this->httpClientService = $httpClientService;
        $this->proxyRepository = $proxyRepository;
        $this->domParser = $domParser;
        $this->couponRepository = $couponRepository;
        $this->sourceRepository = $sourceRepository;
        $this->setSource();
    }

    /**
     * @return void
     * @throws Throwable
     */
    public function fetchSource(): void
    {
        foreach ($this->source->urls as $url) {
            try {
                $pageNode = new DomParser($this->getContent($url), $url, $this->source->baseUrl);
                $this->couponData = new CouponData();
                $this->couponData->setPageNode($pageNode);
                $this->fetchSourcePage();
            } catch (Exception $exception) {
                Log::info($exception->getMessage());
                continue;
            }
        }
    }

    /**
     * @param bool $useProxy
     */
    public function setUseProxy(bool $useProxy): void
    {
        $this->useProxy = $useProxy;
    }

    /***
     * @return Carbon|null
     */
    protected function parseCouponExpires(): ?Carbon
    {
        return null;
    }

    /**
     * @param string $url
     * @param string $method
     * @param array|null $headers
     * @param string|null $body
     *
     * @return string|null
     * @throws Throwable
     */
    protected function getContent
    (
        string $url,
        string $method = 'GET',
        ?array $headers = self::HEADERS,
        ?string $body = null
    ): ?string
    {
        $response = $this->httpRequest($url, $method, $headers, $body);

        return $response ? $response->getContent() : null;
    }

    /**
     * @return bool
     * @throws ParserException
     */
    protected function couponExists(): bool
    {
        $externalId = $this->couponData->getExternalId();

        if (!$externalId) {
            throw new ParserException($this->source, 'Unable to check if coupon exists');
        }

        return $this->couponRepository->existsBySourceAndExternalId($this->source, $externalId);
    }

    /**
     * @return Coupon
     * @throws ParserException
     */
    protected function parseNewCoupon(): Coupon
    {
        $this->couponData->setLink($this->parseCouponLink());

        $coupon = new Coupon();
        $coupon->externalId = $this->couponData->getExternalId();
        $coupon->link = $this->couponData->getLink();
        $coupon->title = mb_substr($this->parseCouponTitle(), 0, 255);
        $coupon->code = $this->parseCouponCode();
        $coupon->expires = $this->parseCouponExpires();
        $coupon->sourceId = $this->source->id;

        return $coupon;
    }

    /**
     * @return Coupon
     * @throws ParserException
     */
    protected function parseExistedCoupon(): Coupon
    {
        $coupon = $this->couponRepository->getBySourceAndExternalId($this->source, $this->couponData->getExternalId());
        $coupon->title = mb_substr($this->parseCouponTitle(), 0, 255);
        $coupon->expires = $this->parseCouponExpires();
        $coupon->synchronized = false;

        return $coupon;
    }

    /**
     * @return void
     */
    protected function fetchCoupon(): void
    {
        try {
            $this->couponData->setExternalId($this->parseCouponExternalId());

            $coupon = $this->couponExists() ? $this->parseExistedCoupon() : $this->parseNewCoupon();
            $coupon->save();

            $this->couponData->unsetCoupon();
        } catch (ParserException $exception) {
            return;
        }
    }

    /**
     * @param string $initialLink
     *
     * @return string
     * @throws ParserException
     */
    protected function getEndLink(string $initialLink): string
    {
        $response = $this->httpRequest($initialLink);

        if (!$response) {
            throw new ParserException($this->source, 'Unable to get end link');
        }

        return $response->getInfo('redirect_url') ?? $response->getInfo('url');
    }

    /**
     * @throws Exception
     */
    protected function setSource(): void
    {
        $shortClassName = (new \ReflectionClass($this))->getShortName();
        $alias = strtolower(str_ireplace('ParserService', '', $shortClassName));
        $source = $this->sourceRepository->getByAlias($alias);

        if (!$source) {
            throw new Exception('No parsing source with alias ' . $alias . ' found.');
        }

        $this->source = $source;
    }

    /**
     * @param string $url
     * @param string $method
     * @param array|null $headers
     * @param string|null $body
     *
     * @return ResponseInterface|null
     */
    protected function httpRequest
    (
        string $url,
        string $method = 'GET',
        ?array $headers = self::HEADERS,
        ?string $body = null
    ): ?ResponseInterface
    {
        if ($this->useProxy) {
            $response = $this->httpRequestWithProxy($url, $method, $headers, $body);
        } else {
            sleep(rand(static::MIN_REQUEST_TIMEOUT, static::MAX_REQUEST_TIMEOUT));

            try {
                $response = $this->httpClientService->request($url, $method, $headers, $body, null);
                $this->checkContent($response->getContent());
            } catch (Throwable $exception) {
                Log::info($exception->getMessage());
                $response = null;
            }

        }

        return $response;
    }

    /**
     * @param string $response
     *
     * @return bool
     */
    protected function checkContent(string $response): bool
    {
        return true;
    }

    /**
     * @param string $url
     * @param string $method
     * @param array|null $headers
     * @param string|null $body
     *
     * @return ResponseInterface|null
     */
    private function httpRequestWithProxy
    (
        string $url,
        string $method,
        ?array $headers,
        ?string $body
    ): ?ResponseInterface
    {
        while ($proxy = $this->proxyRepository->chooseProxyForSource($this->source)) {
            $this->wait($proxy);

            try {
                $response = $this->httpClientService->request($url, $method, $headers, $body, $proxy);
                $this->checkContent($response->getContent());
            } catch (Throwable $exception) {
                Log::info($exception->getMessage());
                $this->proxyRepository->markFails($proxy);
                $response = null;
                continue;
            }

            break;
        }

        return $response ?? null;
    }

    /**
     * @param Proxy $proxy
     */
    private function wait(Proxy $proxy): void
    {
        if (!empty($proxy->lastUsageAt)) {
            /** @var Carbon $nextPossibleUsage */
            $nextPossibleUsage = $proxy->lastUsageAt->addSeconds(rand(static::MIN_REQUEST_TIMEOUT, static::MAX_REQUEST_TIMEOUT));
            $now = Carbon::now();

            if ($nextPossibleUsage->gt($now)) {
                sleep($nextPossibleUsage->diffInSeconds($now));
            }

            $proxy->sources()->updateExistingPivot($this->source->id, ['lastUsageAt' => Carbon::now()]);
        }

        $proxy->sources()->attach($this->source->id, ['lastUsageAt' => Carbon::now()]);
    }
}
