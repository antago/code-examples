<?php

namespace App\Contracts;


/**
 * Interface CouponParser
 * @package App\Contracts
 */
interface CouponParserServiceInterface
{
    /**
     * @return void
     */
    public function fetchSource(): void;

    /**
     * @param bool $useProxy
     */
    public function setUseProxy(bool $useProxy): void;
}
