<?php

namespace App\Repositories;

use App\Coupon;
use App\Source;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class CouponRepository
 * @package App\Repositories
 */
class CouponRepository
{
    /**
     * @param Source $source
     * @param string $externalId
     *
     * @return Coupon|null
     */
    public function getBySourceAndExternalId(Source $source, string $externalId): ?Coupon
    {
        return Coupon::withTrashed()
            ->where('sourceId', $source->id)
            ->where('externalId', $externalId)
            ->first();
    }

    /**
     * @param Source $source
     * @param string $externalId
     *
     * @return bool
     */
    public function existsBySourceAndExternalId(Source $source, string $externalId): bool
    {
        return Coupon::withTrashed()
            ->where('sourceId', $source->id)
            ->where('externalId', $externalId)
            ->exists();
    }

    /**
     * @param Collection $wpExistedCoupons
     *
     * @return Collection
     */
    public function getCouponsToStore(Collection $wpExistedCoupons): Collection
    {
        return Coupon::notSynchronized()->orWhereNotIn('id', $wpExistedCoupons->pluck('parserId'))->get();
    }

    /**
     * @param Source $source
     *
     * @return Coupon|null
     */
    public function getSourceNewest(Source $source): ?Coupon
    {
        return $source->coupons()->orderBy('updated_at', 'DESC')->first();
    }

    /**
     * @param Source $source
     * @param Carbon|null $dateTime
     *
     * @return Collection
     */
    public function getSourceExpiredCoupons(Source $source, Carbon $dateTime = null): Collection
    {
        return $source->coupons()
            ->whereDate('expires', '<', Carbon::now())
            ->when($dateTime, function (Builder $query) use ($dateTime) {
                $query->orWhereDate('updated_at', '<', $dateTime);
            })
            ->get();
    }
}
