<?php


namespace App\DataObjects;

use Symfony\Component\DomCrawler\Crawler as DomParser;

/**
 * Class CouponData
 * @package App\DataObjects
 */
class CouponData
{
    /**
     * @var DomParser|null
     */
    private ?DomParser $pageNode = null;

    /**
     * @var DomParser|null
     */
    private ?DomParser $couponNode = null;

    /**
     * @var string|null
     */
    private ?string $externalId = null;

    /**
     * @var string|null
     */
    private ?string $link = null;

    /**
     * @var string|null
     */
    private ?string $title = null;

    /**
     * @var string|null
     */
    private ?string $code = null;

    /**
     * @param DomParser $pageNode
     */
    public function setPageNode(DomParser $pageNode): void
    {
        $this->pageNode = $pageNode;
    }

    /**
     * @param DomParser $couponNode
     */
    public function setCouponNode(DomParser $couponNode): void
    {
        $this->couponNode = $couponNode;
    }

    /**
     * @return string|null
     */
    public function getExternalId(): ?string
    {
        return $this->externalId;
    }

    /**
     * @param string $externalId
     */
    public function setExternalId(string $externalId): void
    {
        $this->externalId = $externalId;
    }

    /**
     * @return string|null
     */
    public function getLink(): ?string
    {
        return $this->link;
    }

    /**
     * @param string $link
     */
    public function setLink(string $link): void
    {
        $this->link = $link;
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string|null
     */
    public function getCode(): ?string
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode(string $code): void
    {
        $this->code = $code;
    }

    /**
     * @return DomParser|null
     */
    public function getPageNode(): ?DomParser
    {
        return $this->pageNode;
    }

    /**
     * @return DomParser|null
     */
    public function getCouponNode(): ?DomParser
    {
        return $this->couponNode;
    }

    /**
     * @return void
     */
    public function unsetCoupon(): void
    {
        $this->couponNode = $this->link = $this->externalId = $this->title = $this->code = null;
    }
}
