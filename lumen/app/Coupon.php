<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Coupon
 * @package App
 *
 * @property integer $id
 * @property string $title
 * @property string $link
 * @property string|null $code
 * @property Carbon|null $expires
 * @property string|null $externalId
 * @property integer|null $sourceId
 * @property boolean $synchronized
 * @property Carbon|null $updated_at
 *
 * @property string $discount
 */
class Coupon extends Model
{
    use SoftDeletes;

    /**
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * @return BelongsTo
     */
    public function source(): BelongsTo
    {
        return $this->belongsTo(Source::class, 'sourceId');
    }

    /**
     * @return string
     */
    public function getDiscountAttribute(): string
    {
        preg_match('/[0-9]+%/', $this->title, $matches);

        return $matches[0] ?? '';
    }

    /**
     * @param Builder $query
     *
     * @return Builder
     */
    public function scopeNotSynchronized(Builder $query): Builder
    {
        return $query->where('synchronized', false);
    }
}
