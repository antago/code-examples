<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCouponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('coupons', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('shopId');
            $table->string('title');
            $table->string('link');
            $table->string('code')->nullable();
            $table->dateTime('expires')->nullable();
            $table->unsignedSmallInteger('sourceId');
            $table->string('externalId')->nullable();
            $table->timestamps();
            $table->foreign('shopId')->references('id')->on('shops')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('coupons');
    }
}
