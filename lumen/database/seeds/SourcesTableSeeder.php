<?php

use App\Source;
use Illuminate\Database\Seeder;

/**
 * Class SourcesTableSeeder
 */
class SourcesTableSeeder extends Seeder
{
    const SEARCH_QUERIES = [
        'data recovery',
        'Disk Drill',
        'disk drill for mac',
        'disk drill for windows',
        'disk drill pro',
        'disk drill data recovery',
        'Stellar Data Recovery',
        'stellar recovery',
        'stellar phoenix windows data recovery',
        'stellar phoenix photo recovery',
        'stellar phoenix mac data recovery',
        'stellar photo recovery',
        'Wondershare Data Recovery',
        'recoverit data recovery',
        'wondershare recoverit',
        'wondershare data recovery mac',
        'Minitool Power Data Recovery',
        'minitool data recovery',
        'minitool power data recovery mac',
        'iSkysoft Data Recovery',
        'iskysoft recovery',
        'iskysoft data recovery for mac',
        'Data Rescue',
        'prosoft data rescue',
        'data rescue mac',
        'data rescue 4',
        'data rescue 5',
        'R-STUDIO',
        'r-studio data recovery',
        'r-studio recovery',
        'r-studio for mac',
        'Recuva',
        'piriform recuva',
        'recuva professional',
        'recuva for windows',
        'recuva mac',
        'Easeus Data Recovery',
        'easeus data recovery wizard',
        'easeus data recovery wizard professional',
        'easeus data recovery mac',
        'DiskWarrior',
        'diskwarrior mac',
        'alsoft diskwarrior',
        'Ontrack Data Recovery',
        'kroll ontrack data recovery',
        'ontrack easy recovery',
        'ontrack data recovery software',
        'ontrack recovery software'
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        foreach ($this->getSources() as $item) {
            $source = Source::firstOrNew(['alias' => $item['alias']]);
            $source = $source->fill($item);
            $source->save();
        }
    }

    /**
     * @return array
     */
    private function getSources(): array
    {
        $methods = array_filter(get_class_methods($this), fn ($method) => preg_match('/^get(\w+)Source$/', $method));

        return array_map(fn ($method) => $this->$method(), $methods);
    }

    /**
     * @return array
     */
    private function getRetailMeNotSource(): array
    {
        $urls = [];
        $shops = [
            'stellarinfo.com',
            'wondershare.com',
            'iskysoft.com',
            'easeus.com',
        ];

        foreach ($shops as $shop) {
            $urls[] = 'https://www.retailmenot.com/view/' . $shop;
        }

        foreach (self::SEARCH_QUERIES as $query) {
            $urls[] = 'https://www.retailmenot.com/s/' . str_replace(' ', '+', $query);
        }

        return [
            'alias' => 'RetailMeNot',
            'baseUrl' => 'https://www.retailmenot.com',
            'urls' => $urls,
        ];
    }

    /**
     * @return array
     */
    private function getCouponChiefSource(): array
    {
        $urls = [];
        $shops = [
            'cleverfiles',
            'stellarinfo',
            'prosofteng',
            'iskysoft',
            'easeus',
            'wondershare',
        ];

        foreach ($shops as $shop) {
            $urls[] = 'https://www.couponchief.com/' . $shop;
        }

        return [
            'alias' => 'CouponChief',
            'baseUrl' => 'https://www.couponchief.com',
            'urls' => $urls,
        ];
    }

    /**
     * @return array
     */
    private function getDealsCoveSource(): array
    {
        $urls = [];
        $shops = [
            'stellarinfo.com',
            'cleverfiles.com',
            'iskysoft.com',
            'prosofteng.com',
            'easeus.com',
            'wondershare.com',
            'wondershare.net',
        ];

        foreach ($shops as $shop) {
            $urls[] = 'http://www.dealscove.com/coupons/' . $shop;
        }

        return [
            'alias' => 'DealsCove',
            'baseUrl' => 'http://www.dealscove.com',
            'urls' => $urls,
        ];
    }

    /**
     * @return array
     */
    private function getGivingAssistantSource(): array
    {
        $urls = [];
        $shops = [
            'stellarinfo.com',
            'wondershare.com',
        ];

        foreach ($shops as $shop) {
            $urls[] = 'https://givingassistant.org/coupon-codes/' . $shop;
        }

        return [
            'alias' => 'GivingAssistant',
            'baseUrl' => 'https://givingassistant.org',
            'urls' => $urls,
        ];
    }

    /**
     * @return array
     */
    private function getGrouponSource(): array
    {
        $urls = [];
        $shops = [
            'stellar-data-recovery',
            'wondershare',
        ];

        foreach ($shops as $shop) {
            $urls[] = 'https://www.groupon.com/coupons/' . $shop;
        }

        return [
            'alias' => 'Groupon',
            'baseUrl' => 'https://www.groupon.com',
            'urls' => $urls,
        ];
    }

    /**
     * @return array
     */
    private function getJoinHoneySource(): array
    {
        $urls = [];
        $shops = [
            'stellar',
            'wondershare',
            'isky-soft',
            'prosoft-engineering',
            'ccleaner',
            'easeus',
        ];

        foreach ($shops as $shop) {
            $urls[] = 'https://www.joinhoney.com/shop/' . $shop;
        }

        return [
            'alias' => 'JoinHoney',
            'baseUrl' => 'https://www.joinhoney.com',
            'urls' => $urls,
        ];
    }

    /**
     * @return array
     */
    private function getSlickDealsSource(): array
    {
        $urls = [];
        $shops = [
            'stellar-data-recovery',
            'wondershare',
            'iskysoft',
            'prosofteng',
        ];

        foreach ($shops as $shop) {
            $urls[] = 'https://slickdeals.net/coupons/' . $shop . '/';
        }

        return [
            'alias' => 'SlickDeals',
            'baseUrl' => 'https://slickdeals.net',
            'urls' => $urls,
        ];
    }

    /**
     * @return array
     */
    private function getCouponsSource(): array
    {
        $urls = [];
        $shops = [
            'cleverfiles',
            'stellar-data-recovery',
            'wondershare',
            'prosoft-engineering-inc',
            'ccleaner',
            'easeus.com'
        ];

        foreach ($shops as $shop) {
            $urls[] = 'https://www.coupons.com/coupon-codes/' . $shop . '/';
        }

        return [
            'alias' => 'Coupons',
            'baseUrl' => 'https://www.coupons.com',
            'urls' => $urls,
        ];
    }
}
