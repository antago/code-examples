<?php

namespace Services;

use App\Services\HttpClientService;
use App\Services\SiteLogoParserService;
use Mockery;
use Mockery\MockInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Contracts\HttpClient\ResponseInterface;

class SiteLogoParserServiceTest extends TestCase
{
    /**
     * @var string
     */
    private string $url;

    /**
     * @var MockInterface
     */
    private MockInterface $httpClient;

    /**
     * @var MockInterface
     */
    private MockInterface $response;

    protected function setUp(): void
    {
        parent::setUp();
        $this->url = 'http://test.com';
        $this->response = Mockery::mock(ResponseInterface::class);
        $this->httpClient = Mockery::mock(HttpClientService::class);
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        Mockery::close();
    }

    public function testCanParseLogoImageTitledAsLogo(): void
    {
        $src = 'logo.png';
        $html = '<div>
                    <a href="/"><img src="'. $src . '" alt="logo"></a>
                </div>';

        $this->arrange($html, $src, 200);

        /** @var SiteLogoParserService $service */
        $service = new SiteLogoParserService($this->httpClient);

        $this->assertEquals($this->url . '/' . $src, $service->parseLogo($this->url));
    }

    public function testCanParseLogoImageBySelectors(): void
    {
        $src = 'image.png';
        $html = '<div>
                    <a href="/"><img src="' . $src . '" alt="logo"></a>
                </div>';

        $this->arrange($html, $src, 200);

        /** @var SiteLogoParserService $service */
        $service = new SiteLogoParserService($this->httpClient);

        $this->assertEquals($this->url . '/' . $src, $service->parseLogo($this->url));
    }

    public function testCanParseFavicon(): void
    {
        $src = 'favicon.ico';
        $html = '<div>
                  <link rel="icon" href="' . $src . '">
                </div>';

        $this->arrange($html, $src, 200);

        /** @var SiteLogoParserService $service */
        $service = new SiteLogoParserService($this->httpClient);

        $this->assertEquals($this->url . '/' . $src, $service->parseLogo($this->url));
    }

    public function testReturnNullWhenLogoNotFound(): void
    {
        $html = '<div>
                    <span></span>
                </div>';

        $this->arrange($html, null, 400);

        /** @var SiteLogoParserService $service */
        $service = new SiteLogoParserService($this->httpClient);

        $this->assertNull($service->parseLogo($this->url));
    }

    /**
     * @param string $html
     * @param string $src
     * @param int $code
     */
    private function arrange(string $html, ?string $src = null, int $code = 200): void
    {
        $this->response->shouldReceive('getStatusCode')->andReturn($code);

        if ($src) {
            $this->response->shouldReceive('getInfo')->andReturn($this->url . '/' . $src);
        }

        $this->httpClient->shouldReceive('requestGetContent')->andReturn($html);
        $this->httpClient->shouldReceive('requestGetStatus')->andReturn($code);
        $this->httpClient->shouldReceive('request')->andReturn($this->response);
    }
}
