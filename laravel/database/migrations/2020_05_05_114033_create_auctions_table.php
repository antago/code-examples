<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAuctionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('auctions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('created_by');
            $table->timestamp('datetime_start');
            $table->timestamp('datetime_end');
            $table->boolean('prolongation_allowed')->default(false);
            $table->unsignedTinyInteger('prolongation_step')->nullable();
            $table->timestamp('datetime_max_end')->nullable();
            $table->unsignedInteger('bid_step');
            $table->unsignedInteger('bid_step_percent');
            $table->boolean('instant_buy_allowed')->default(false);
            $table->unsignedInteger('instant_price')->nullable();
            $table->boolean('splittable')->default(false);
            $table->unsignedInteger('min_split_count')->nullable();
            $table->unsignedTinyInteger('status');
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('updated_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('auctions', function (Blueprint $table) {
            $table->dropForeign('auctions_created_by_foreign');
            $table->dropForeign('auctions_updated_by_foreign');
        });
        Schema::dropIfExists('auctions');
    }
}
