<?php

declare(strict_types=1);

namespace App\Policies;

use App\Auction;
use App\Role;
use App\Services\AuctionService;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

/**
 * Class AuctionPolicy
 * @package App\Policies
 */
class AuctionPolicy
{
    use HandlesAuthorization;

    /**
     * @var AuctionService
     */
    private AuctionService $auctionService;

    /**
     * AuctionPolicy constructor.
     *
     * @param AuctionService $auctionService
     */
    public function __construct(AuctionService $auctionService)
    {
        $this->auctionService = $auctionService;
    }

    /**
     * @param User $user
     *
     * @return bool|void
     */
    public function before(User $user)
    {
        if ($user->isAdmin()) {
            return true;
        }
    }

    /**
     * Determine whether the user can view the auction.
     *
     * @param User $user
     * @param Auction $auction
     *
     * @return bool
     */
    public function view(User $user, Auction $auction): bool
    {
        return $this->clientCanView($user, $auction) || $this->transporterCanView($user, $auction);
    }

    /**
     * Determine whether the user can create auctions.
     *
     * @param User $user
     * @param int[] $orders
     *
     * @return bool
     */
    public function create(User $user, array $orders): bool
    {
        return $user->hasRole(Role::STAFF_ROLE_CLIENT)
            && ! array_diff($orders, $user->company->orders->pluck('id')->toArray());
    }

    /**
     * @param User $user
     * @return bool
     */
    public function viewCreatingForm(User $user): bool
    {
        return $user->hasRole(Role::STAFF_ROLE_CLIENT);
    }

    /**
     * Determine whether the user can update the auction.
     *
     * @param User $user
     * @param Auction $auction
     * @param int[] $orders
     *
     * @return bool
     */
    public function update(User $user, Auction $auction, array $orders): bool
    {
        return $this->clientCanView($user, $auction)
            && ! array_diff($orders, $user->company->orders->pluck('id')->toArray());
    }

    /**
     * @param User $user
     * @param Auction $auction
     *
     * @return bool
     */
    public function edit(User $user, Auction $auction): bool
    {
        return $user->hasRole(Role::STAFF_ROLE_CLIENT) && $this->clientCanView($user, $auction);
    }

    /**
     * @param User $user
     * @param Auction $auction
     *
     * @return bool
     */
    public function finishOrCancel(User $user, Auction $auction): bool
    {
        return $this->clientCanView($user, $auction);
    }

    /**
     * @param User $transporter
     * @param Auction $auction
     *
     * @return bool
     */
    private function transporterCanView(User $transporter, Auction $auction): bool
    {
        return $auction->status != Auction::STATUS_WAIT
             && $this->auctionService->auctionIsAllowedForTransporter($auction, $transporter->company);
    }

    /**
     * @param User $client
     * @param Auction $auction
     *
     * @return bool
     */
    private function clientCanView(User $client, Auction $auction): bool
    {
        $clientUsers = $client->company->users->pluck('id');

        return $client->hasRole(Role::STAFF_ROLE_CLIENT)
            && $clientUsers->contains($auction->created_by);
    }
}
