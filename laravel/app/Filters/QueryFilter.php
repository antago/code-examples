<?php

declare(strict_types = 1);

namespace App\Filters;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class QueryFilter
 * @package App\Filters
 */
abstract class QueryFilter
{
    const PHONE_LENGTH = 11;

    const INN_INDIVIDUAL_ENTREPRENEUR = 12;
    const INN_LEGAL_ENTITY = 10;
    const MYSQL_INT_MAX = 2147483647;

    /**
     * @var Builder
     */
    protected Builder $builder;

    /**
     * @var Request
     */
    protected Request $request;

    /**
     * QueryFilter constructor.
     *
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * @param Builder $builder
     *
     * @return Builder
     */
    public function apply(Builder $builder): Builder
    {
        $this->builder = $builder;

        $this->preFiltering();

        foreach ($this->filters() as $filter => $value) {
            $filter = str_replace('_', '', $filter);

            if (method_exists($this, $filter)) {
                $this->$filter($value);
            }
        }

        return $this->builder;
    }

    /**
     * @return array
     */
    public function filters(): array
    {
        return $this->request->all();
    }

    /**
     * @return void
     */
    protected function preFiltering(): void
    {
    }

    /**
     * @param $value
     *
     * @return bool
     */
    public function looksLikeId($value): bool
    {
        return is_numeric($value) && $value < self::MYSQL_INT_MAX;
    }

    /**
     * @param $value
     *
     * @return bool
     */
    public function looksLikeInn($value): bool
    {
        return looksLikeInn($value);
    }

    /**
     * @param $value
     *
     * @return bool
     */
    public function looksLikePhone($value): bool
    {
        return preg_match("/^[\+0-9\-\(\)\s]*$/", $value) && strlen($value) >= self::PHONE_LENGTH;
    }

    /**
     * @param $value
     *
     * @return bool
     */
    public function looksLikeCarNumber($value): bool
    {
        $formattedValue = str_replace(' ', '', $value);

        return preg_match("/^[A-Z|А-Я]{2}[0-9]{3}[A-Z|А-Я]{4}[0-9]{2,3}$/i", $formattedValue)
            || preg_match("/^[0-9]{3}$/", $formattedValue)
            || preg_match("/^[A-Z|А-Я]{2}[0-9]{3}[A-Z|А-Я]{4}$/i", $formattedValue);
    }

    /**
     * @param $value
     *
     * @return bool
     */
    public function looksLikeTrailerNumber($value): bool
    {
        $formattedValue = str_replace(' ', '', $value);

        return preg_match("/^[A-Z|А-Я]{4}[0-9]{6}$/i", $formattedValue)
            || preg_match("/^[0-9]{4}$/", $formattedValue)
            || preg_match("/^[A-Z|А-Я]{4}[0-9]{4}$/i", $formattedValue);
    }
}
