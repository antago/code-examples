<?php

declare(strict_types = 1);

namespace App\Filters;

use App\Http\Requests\Auctions\IndexAuctionRequest;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class AuctionsFilter
 * @package App\Filters
 */
class AuctionsFilter extends QueryFilter
{
    /**
     * QueryFilter constructor.
     *
     * @param IndexAuctionRequest $request
     */
    public function __construct(IndexAuctionRequest $request)
    {
        parent::__construct($request);
    }

    /**
     * @param string $value
     */
    public function dateStart(string $value): void
    {
        $this->builder->filterDateStart($value, $this->request->has('date_end'));
    }

    /**
     * @param string $value
     */
    public function dateEnd(string $value): void
    {
        $this->builder->filterDateEnd($value, $this->request->has('date_start'));
    }

    /**
     * @param array $values
     */
    public function status(array $values): void
    {
        $this->builder->filterStatus($values);
    }

    /**
     * @param array $values
     */
    public function clients(array $values): void
    {
        $this->builder->filterClients($values);
    }

    /**
     * @param $value
     */
    public function search($value): void
    {
        $this->builder->where(function (Builder $query) use ($value) {
            $query->orWhere(function (Builder $query) use ($value) {
                $query->where('auctions.id', $value);
            });

            $query->orWhere(function (Builder $query) use ($value) {
                $query->filterOrderId($value);
            });
        });
    }
}

