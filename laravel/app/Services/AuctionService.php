<?php

declare(strict_types = 1);

namespace App\Services;

use App\Http\Requests\Auctions\{
    CancelAuctionRequest,
    FinishAuctionRequest,
    InstantBuyAuctionRequest,
    StoreAuctionRequest,
    UpdateAuctionRequest,
    StoreBidRequest
};
use App\Repository\{AuctionRepository, OrderRepository, BidRepository};
use App\{Auction, Bid, Company, Events\AuctionStartedEvent, Exceptions\NotAllowedException, Role, Order, User};
use App\Filters\AuctionsFilter;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;

/**
 * Class AuctionService
 * @package App\Services
 */
class AuctionService
{
    /**
     * @var AuctionRepository
     */
    private AuctionRepository $auctionRepository;

    /**
     * @var OrderRepository
     */
    private OrderRepository $orderRepository;

    /**
     * @var BidRepository
     */
    private BidRepository $bidRepository;

    /**
     * @var OrderService
     */
    private OrderService $orderService;

    /**
     * AuctionService constructor.
     *
     * @param AuctionRepository $auctionRepository
     * @param OrderRepository $orderRepository
     * @param BidRepository $bidRepository
     * @param OrderService $orderService
     */
    public function __construct
    (
        AuctionRepository $auctionRepository,
        OrderRepository $orderRepository,
        BidRepository $bidRepository,
        OrderService $orderService
    )
    {
        $this->auctionRepository = $auctionRepository;
        $this->orderRepository = $orderRepository;
        $this->bidRepository = $bidRepository;
        $this->orderService = $orderService;
    }

    /**
     * @param AuctionsFilter $filter
     *
     * @return Collection
     */
    public function getList(AuctionsFilter $filter): Collection
    {
        $user = auth()->user();

        if ($user->isAdmin()) {
            $list = $this->auctionRepository->getListForAdmin($filter);
        } elseif ($user->hasRole(Role::STAFF_ROLE_CLIENT)) {
            $list = $this->auctionRepository->getListForClient($filter, $user->company);
        } else {
            $list = $this->auctionRepository->getListForTransporter($filter, $user->company);
        }

        $statusLabels = Auction::getStatusLabels();
        $productUnitTypes = Order::getUnitLabels();
        $list->getCollection()->each->setAppends(['totalAmount', 'rate', 'totalQuantity', 'rateType', 'quantityType']);

        return collect(compact('list', 'statusLabels', 'productUnitTypes'));
    }

    /**
     * @param Auction $auction
     *
     * @return Collection
     */
    public function getBidsList(Auction $auction): Collection
    {
        return $this->anonymizeBids($this->bidRepository->getList($auction))->sortByDesc('created_at')->values();
    }

    /**
     * @param StoreAuctionRequest $request
     *
     * @return void
     */
    public function store(StoreAuctionRequest $request): void
    {
        $data = $request->validated();

        DB::transaction(function () use ($data) {
            $auction = $this->auctionRepository->create(auth()->user(), $data);
            $auction->transporters()->sync($data['transporters']);
            $auction->orders()->sync($data['orders']);

            event(new AuctionStartedEvent(
                $auction,
                $auction->transporters()->exists()
                    ? User::whereIn('company_id', $auction->transporters->pluck('id')->toArray())->get()
                    : User::role('transporter')->get()
            ));
        });
    }

    /**
     * @param Auction $auction
     *
     * @return Auction
     */
    public function show(Auction $auction): Auction
    {
        return $auction->load('orders', 'transporters');
    }

    /**
     * @param UpdateAuctionRequest $request
     * @param Auction $auction
     *
     * @return void
     */
    public function update(UpdateAuctionRequest $request, Auction $auction): void
    {
        $data = $request->validated();

        DB::transaction(function () use ($data, $auction) {
            $this->auctionRepository->update($auction, auth()->user(), $data);
            $auction->transporters()->sync($data['transporters']);
            $auction->orders()->sync($data['orders']);
        });
    }

    /**
     * @param StoreBidRequest $request
     * @param Auction $auction
     */
    public function makeBid(StoreBidRequest $request, Auction $auction): void
    {
        $data = $request->validated();
        $data['auction_id'] = $auction->id;
        $data['bidder_id'] = auth()->user()->company_id;
        $data['rate_type'] = $auction->bidRateType;

        $this->bidRepository->create($data);
    }

    /**
     * @param Auction $auction
     *
     * @return Bid|null
     */
    public function getCurrentBid(Auction $auction): ?Bid
    {
        return $this->bidRepository->getCurrentBid($auction);
    }

    /**
     * @param Auction $auction
     * @param Company $bidder
     *
     * @return Bid|null
     */
    public function getBidderCurrentBid(Auction $auction, Company $bidder): ?Bid
    {
        return $this->bidRepository->getBidderCurrentBid($auction, $bidder);
    }

    /**
     * @param Auction $auction
     * @param Company $transporter
     *
     * @return bool
     */
    public function auctionIsAllowedForTransporter(Auction $auction, Company $transporter): bool
    {
        return $auction->transporters->count() == 0 || $auction->transporters->contains($transporter);
    }

    public function startOrFinishAuctions(): void
    {
        $expiredAuctions = $this->auctionRepository->getExpired();
        $neededToStartAuctions = $this->auctionRepository->getNeededToStart();

        foreach ($expiredAuctions as $auction) {
            $this->finishAuction($auction);
        }

        foreach ($neededToStartAuctions as $auction) {
            $this->startAuction($auction);
        }
    }

    /**
     * @param Auction $auction
     * @param FinishAuctionRequest $request
     */
    public function finishAuctionManually(Auction $auction, FinishAuctionRequest $request): void
    {
        $this->finishAuction($auction, auth()->user(), $request->only('end_reason'));
    }

    /**
     * @param Auction $auction
     *
     * @throws NotAllowedException
     */
    public function startAuction(Auction $auction): void
    {
        if ($auction->status != Auction::STATUS_WAIT) {
            throw new NotAllowedException();
        }

        $auction->status = Auction::STATUS_ACTIVE;
        $this->auctionRepository->update($auction, auth()->user());
    }

    /**
     * @param Auction $auction
     * @param CancelAuctionRequest $request
     */
    public function cancelAuction(Auction $auction, CancelAuctionRequest $request): void
    {
        $data = $request->only('end_reason');
        $auction->status = Auction::STATUS_CANCELED;
        $this->auctionRepository->update($auction, auth()->user(), $data);
    }

    /**
     * @param Auction $auction
     * @param InstantBuyAuctionRequest $request
     */
    public function instantBuy(Auction $auction, InstantBuyAuctionRequest $request): void
    {
        $transporter = auth()->user()->company;

        $data = $request->validated();
        $data['auction_id'] = $auction->id;
        $data['bidder_id'] = $transporter->id;
        $data['is_instant_buy'] = true;
        $data['rate'] = $auction->instant_price;
        $data['rate_type'] = $auction->bidRateType;
        $data['quantity'] = $auction->totalQuantity;

        $this->bidRepository->create($data);
        $this->finishAuction($auction);
    }

    /**
     * @param Auction $auction
     * @param User|null $user
     * @param array|null $data
     */
    private function finishAuction(Auction $auction, ?User $user = null, ?array $data = []): void
    {
        $auction->status = Auction::STATUS_FINISHED;
        $this->auctionRepository->update($auction, $user, $data);
        Artisan::call('determinate:winner');
    }

    /**
     * @param array $data
     *
     * @return array
     */
    private function filterUpdatingData(array $data): array
    {
        $protectedColumns = ['splittable'];

        return array_filter($data, fn($column, $key) => !in_array($key, $protectedColumns), ARRAY_FILTER_USE_BOTH);
    }

    /**
     * @param Collection|Bid[] $bids
     *
     * @return Collection|Bid[]
     */
    private function anonymizeBids(Collection $bids): Collection
    {
        $biddersMap = [];
        $counter = 1;

        return $bids->transform(function (Bid $bid) use (&$biddersMap, &$counter) {
            $bid->isCurrentUser = false;

            if (!empty($biddersMap[$bid->bidder_id])) {
                $bid->bidder_name = $biddersMap[$bid->bidder_id];
            } else {
                $bid->bidder_name = 'Перевозчик №' . $counter;
                $biddersMap[$bid->bidder_id] = $bid->bidder_name;
                $counter++;
            }

            return $bid;
        })->transform(function (Bid $bid) {
            if (auth()->user()->company_id == $bid->bidder_id) {
                $bid->bidder_name = $bid->bidder->name;
                $bid->isCurrentUser = true;
            }

            unset($bid->bidder, $bid->bidder_id);

            return $bid;
        });
    }

    /**
     * @param Auction $auction
     *
     * @return Collection
     */
    public function formatOrdersForEmail(Auction $auction): Collection
    {
        $formattedOrders = new Collection();

        /** @var Order $order */
        foreach ($auction->orders as $order) {
            $product = $order->product;
            $formattedOrders->push([
                'id' => $order->id,
                'capacity' => $order->capacity,
                'capacityType' => Order::getCurrencyStatusLabels($order->product_unit_type),
                'product' => $product->name,
                'productType' => $product->productType->name,
                'rate' => $order->transporter_rate . ' ' . Order::getProductStatusLabels($order->currency_unit_type),
                'orderCost' => $order->average_order_cost,
            ]);
        }

        return $formattedOrders;
    }

    /**
     * @param Auction $auction
     *
     * @return Collection
     */
    public function getHistory(Auction $auction): Collection
    {
        return $auction->getHistory()->values();
    }
}
