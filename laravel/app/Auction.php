<?php

declare(strict_types=1);

namespace App;

use App\Traits\Filterable;
use Carbon\Carbon;
use App\Traits\Loggable;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Auction
 * @package App
 *
 * @property int $id
 * @property Carbon $datetime_start
 * @property Carbon $datetime_end
 * @property bool $prolongation_allowed
 * @property int $prolongation_step
 * @property Carbon $datetime_max_end
 * @property int $bid_step
 * @property int $bid_step_percent
 * @property bool $instant_buy_allowed
 * @property int $instant_price
 * @property bool $splittable
 * @property int $min_split_count
 * @property int $status
 * @property int $instant_buyer_id
 * @property Carbon $created_at
 * @property Carbon|null $updated_at
 * @property Carbon|null $deleted_at
 * @property int $created_by
 * @property int $updated_by
 *
 * @property int $totalAmount
 * @property int $rate
 * @property int $rateType
 * @property int $totalQuantity
 * @property int $quantityType
 * @property int $bidRateType
 * @property int $totalRate
 * @property Collection|Order[] $orders
 */
class Auction extends Model
{
    use SoftDeletes, Filterable, Loggable;

    const PAGINATE_LIMIT = 20;

    const STATUS_WAIT = 1;
    const STATUS_ACTIVE = 2;
    const STATUS_FINISHED = 3;
    const STATUS_CANCELED = 4;

    /**
     * Columns for which history is recorded
     */
    const LOGGABLE_COLUMNS = [
        'datetime_start', 'datetime_end', 'prolongation_allowed', 'prolongation_step', 'datetime_max_end',
        'bid_step', 'bid_step_percent', 'instant_buy_allowed', 'instant_price', 'splittable', 'min_split_count',
        'status', 'end_reason', 'deleted_at'
    ];

    /**
     * Columns for which history is displayed
     */
    const LOGGABLE_MAP = [
        'datetime_start' => [],
        'datetime_end' => [],
        'prolongation_allowed' => [],
        'prolongation_step' => [],
        'datetime_max_end' => [],
        'bid_step' => [],
        'bid_step_percent' => [],
        'instant_buy_allowed' => [],
        'instant_price' => [],
        'splittable' => [],
        'min_split_count' => [],
        'status' => [],
        'end_reason' => [],
    ];

    /**
     * @var string
     */
    protected string $historyModel = AuctionHistory::class;

    /**
     * @var string[]
     */
    protected $fillable = [
        'datetime_start', 'datetime_end', 'prolongation_allowed', 'prolongation_step',
        'datetime_max_end', 'bid_step', 'bid_step_percent', 'instant_buy_allowed',
        'instant_price', 'splittable', 'min_split_count', 'end_reason',
    ];

    /**
     * @param int|null $status
     *
     * @return array|mixed
     */
    public static function getStatusLabels(?int $status = null)
    {
        return getModelConstantsLabels([
            self::STATUS_WAIT => [
                'id' => self::STATUS_WAIT,
                'name' => 'Ожидает начала',
            ],
            self::STATUS_ACTIVE => [
                'id' => self::STATUS_ACTIVE,
                'name' => 'Идут торги',
            ],
            self::STATUS_FINISHED => [
                'id' => self::STATUS_FINISHED,
                'name' => 'Завершен',
            ],
            self::STATUS_CANCELED => [
                'id' => self::STATUS_CANCELED,
                'name' => 'Отменен',
            ],
        ], $status);
    }

    /**
     * @return BelongsTo
     */
    public function creator(): BelongsTo
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    /**
     * @return BelongsToMany
     */
    public function transporters(): BelongsToMany
    {
        return $this->belongsToMany(
            Company::class,
            'auction_transporter',
            'auction_id',
            'transporter_id'
        );
    }

    /**
     * @return BelongsToMany
     */
    public function orders(): BelongsToMany
    {
        return $this->belongsToMany(Order::class);
    }

    /**
     * @return HasMany
     */
    public function bids(): HasMany
    {
        return $this->hasMany(Bid::class);
    }

    /**
     * @return BelongsTo
     */
    public function instantBuyer(): BelongsTo
    {
        return $this->belongsTo(Company::class, 'instant_buyer_id');
    }

    /**
     * @param Builder $query
     * @param $value
     * @param bool $period
     *
     * @return Builder
     */
    public function scopeFilterDateStart(Builder $query, $value, $period = false): Builder
    {
        return $period
            ? $query->whereDate('datetime_end', '>=', $value)
            : $query->whereDate('datetime_start', '<=', $value)
                ->whereDate('datetime_end', '>=', $value);
    }

    /**
     * @param Builder $query
     * @param $value
     * @param bool $period
     *
     * @return Builder
     */
    public function scopeFilterDateEnd(Builder $query, $value, $period = false): Builder
    {
        return $period
            ? $query->whereDate('datetime_start', '<=', $value)
            : $query->whereDate('datetime_start', '<=', $value)
                ->whereDate('datetime_end', '>=', $value);
    }

    /**
     * @param Builder $query
     * @param array $values
     *
     * @return Builder
     */
    public function scopeFilterStatus(Builder $query, array $values): Builder
    {
        return $query->whereIn('status', $values);
    }

    /**
     * @param Builder $query
     * @param array $values
     *
     * @return Builder
     */
    public function scopeFilterClients(Builder $query, array $values): Builder
    {
        return $query->whereHas('orders', function (Builder $query) use ($values) {
            $query->whereIn('client_id', $values);
        });
    }

    /**
     * @param Builder $query
     * @param $value
     *
     * @return Builder
     */
    public function scopeFilterOrderId(Builder $query, $value): Builder
    {
        return $query->whereHas('orders', function (Builder $query) use ($value) {
            $query->where('orders.id', $value);
        });
    }

    /**
     * @param Builder $query
     * @param int $value
     *
     * @return Builder
     */
    public function scopeFilterTransporter(Builder $query, int $value): Builder
    {
        return $query->whereHas('transporters', function (Builder $query) use ($value) {
            $query->where('companies.id', $value);
        })->orWhereDoesntHave('transporters');
    }

    /**
     * @param Builder $query
     *
     * @return Builder
     */
    public function scopeActual(Builder $query): Builder
    {
        return $query->where('status', '!=', Auction::STATUS_CANCELED);
    }

    /**
     * @return float
     */
    public function getTotalAmountAttribute(): float
    {
        return (float)$this->orders->sum('average_order_cost');
    }

    /**
     * @return float|null
     */
    public function getRateAttribute(): ?float
    {
        return (float)($this->orders->count() == 1 ? $this->orders->first()->transporter_rate : null);
    }

    /**
     * @return int|null
     */
    public function getRateTypeAttribute(): ?int
    {
        return $this->orders->count() > 1 ? null : $this->orders->first()->currency_unit_type;
    }

    /**
     * @return int|null
     */
    public function getTotalQuantityAttribute(): ?int
    {
        return $this->orders->unique('product_unit_type')->count() > 1
            ? null
            : $this->orders->sum('capacity');
    }

    /**
     * @return int|null
     */
    public function getQuantityTypeAttribute(): ?int
    {
        return $this->orders->unique('product_unit_type')->count() == 1
            ? $this->orders->first()->product_unit_type
            : null;
    }

    /**
     * @return int
     */
    public function getBidRateTypeAttribute(): int
    {
        return $this->orders->count() > 1 ? Order::RATE_TYPE_PER_ALL : Order::RATE_TYPE_PER_ITEM;
    }

    /**
     * @return float
     */
    public function getTotalRateAttribute(): float
    {
        return $this->orders->count() > 1
            ? (float)$this->orders->sum('average_order_cost')
            : (float)$this->orders->first()->transporter_rate;
    }
}
