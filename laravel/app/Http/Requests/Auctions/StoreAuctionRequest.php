<?php

declare(strict_types = 1);

namespace App\Http\Requests\Auctions;

use App\Http\Requests\FormRequest;
use App\Rules\AuctionMinSplitCount;
use App\Rules\InstantPriceIsLowerThanOrders;
use App\Rules\OrdersAreAcceptableForAuction;
use App\Rules\TransportersAreAcceptableForAuction;

/**
 * Class StoreAuctionRequest
 * @package App\Http\Requests\Auctions
 */
class StoreAuctionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'datetime_start' => 'required|date_format:"Y-m-d H:i"|after:now',
            'datetime_end' => 'required|date_format:"Y-m-d H:i"|after:' . $this->input('datetime_start'),
            'prolongation_allowed' => 'nullable|boolean',
            'prolongation_step' => 'nullable|integer|required_if:prolongation_allowed,1',
            'datetime_max_end' => 'nullable|date_format:"Y-m-d H:i"|after:' . $this->input('datetime_end'),
            'bid_step' => 'required|numeric',
            'bid_step_percent' => 'required|integer',
            'instant_buy_allowed' => 'nullable|boolean',
            'instant_price' => ['nullable', 'numeric', 'required_if:instant_buy_allowed,1', new InstantPriceIsLowerThanOrders],
            'splittable' => 'nullable|boolean',
            'min_split_count' => ['nullable', 'integer', 'required_if:splittable,1', new AuctionMinSplitCount],
            'transporters' => ['nullable', 'array', new TransportersAreAcceptableForAuction],
            'transporters.*' => 'integer|exists:companies,id',
            'orders' => ['required', 'array', new OrdersAreAcceptableForAuction],
            'orders.*' => 'integer|exists:orders,id',
        ];
    }

    /**
     * @return array
     */
    public function messages(): array
    {
        return [
            'orders.required' => 'Нельзя создать редукцион без заказов',
            'bid_step.required' => 'Обязательно для заполнения',
            'bid_step_percent.required' => 'Обязательно для заполнения',
            'datetime_start.required' => 'Обязательно для заполнения',
            'datetime_start.after' => 'Дата и время начала торгов должны быть позже текущего времени',
            'datetime_end.required' => 'Обязательно для заполнения',
            'datetime_max_end.after' => 'Значение поля должно быть позже окончания торгов',
            'datetime_end.after' => 'Значение поля должно быть позже начала торгов',
            'datetime_end.date_format' => 'Неверный формат даты и времени',
            'datetime_max_end.date_format' => 'Неверный формат даты и времени',
        ];
    }
}
