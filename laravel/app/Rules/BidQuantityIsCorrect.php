<?php

declare(strict_types=1);

namespace App\Rules;

use App\Auction;
use App\Services\AuctionService;
use Illuminate\Contracts\Validation\Rule;

/**
 * Class BidQuantityIsCorrect
 * @package App\Rules
 */
class BidQuantityIsCorrect implements Rule
{
    /**
     * @var AuctionService
     */
    private AuctionService $auctionService;

    /**
     * @var string
     */
    private string $message = '';

    /**
     * BidIsCorrect constructor.
     */
    public function __construct()
    {
        $this->auctionService = app(AuctionService::class);
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     *
     * @return bool
     */
    public function passes($attribute, $value): bool
    {
        /** @var Auction $auction */
        $auction = request()->auction;
        $bidderCurrentBid = $this->auctionService->getBidderCurrentBid($auction, auth()->user()->company);
        $lastBidQuantity = $bidderCurrentBid ? $bidderCurrentBid->quantity : $auction->totalQuantity;

        if ($value < $auction->min_split_count) {
            $this->message = 'Минимальное количество груза для участия в аукционе - ' . $auction->min_split_count;
        }

        if ($value > $auction->totalQuantity) {
            $this->message = 'Количество груза в ставке не должно превышать количество грузка в заказе';
        }

        if (($lastBidQuantity - $value) % $auction->min_split_count != 0) {
            $this->message = 'Количество грузка в ставках должно меняться кратно шагу минимальному количеству';
        }

        return ! (bool)$this->message;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message(): string
    {
        return $this->message;
    }
}
