<?php

declare(strict_types = 1);

namespace App\Repository;

use App\{Auction, Company, User};
use App\Filters\AuctionsFilter;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;

/**
 * Class AuctionRepository
 * @package App\Repository
 */
class AuctionRepository
{
    /**
     * @param AuctionsFilter $filter
     *
     * @return LengthAwarePaginator
     */
    public function getListForAdmin(AuctionsFilter $filter): LengthAwarePaginator
    {
        return Auction::filter($filter)
            ->with('orders')
            ->orderBy('created_at', 'desc')
            ->paginate(Auction::PAGINATE_LIMIT);
    }

    /**
     * @param AuctionsFilter $filter
     * @param Company $company
     *
     * @return LengthAwarePaginator
     */
    public function getListForClient(AuctionsFilter $filter, Company $company): LengthAwarePaginator
    {
        return $company->clientAuctions()
            ->filter($filter)
            ->with('orders')
            ->orderBy('created_at', 'desc')
            ->paginate(Auction::PAGINATE_LIMIT);
    }

    /**
     * @param AuctionsFilter $filter
     * @param Company $company
     *
     * @return LengthAwarePaginator
     */
    public function getListForTransporter(AuctionsFilter $filter, Company $company): LengthAwarePaginator
    {
        return Auction::filter($filter)
            ->filterTransporter($company->id)
            ->with('orders')
            ->where('status', '!=', Auction::STATUS_WAIT)
            ->orderBy('created_at', 'desc')
            ->paginate(Auction::PAGINATE_LIMIT);
    }

    /**
     * @param User $user
     * @param array $data
     *
     * @return Auction
     */
    public function create(User $user, array $data): Auction
    {
        $auction = new Auction($data);
        $auction->created_by = $user->id;
        $auction->updated_by = $user->id;
        $auction->status = Auction::STATUS_WAIT;
        $auction->prolongation_step = $auction->prolongation_allowed ? $auction->prolongation_step : null;
        $auction->save();

        return $auction;
    }

    /**
     * @param Auction $auction
     * @param User|null $user
     * @param array|null $data
     *
     * @return Auction
     */
    public function update(Auction $auction, ?User $user = null, ?array $data = []): Auction
    {
        $auction->fill($data);
        $auction->prolongation_step = $auction->prolongation_allowed ? $auction->prolongation_step : null;
        $auction->updated_by = $user->id ?? null;
        $auction->save();

        return $auction;
    }

    /**
     * @return Collection|Auction[]
     */
    public function getExpired(): Collection
    {
        return Auction::where('status', Auction::STATUS_ACTIVE)
            ->where('datetime_end', '<=', Carbon::now())
            ->where(function (Builder $query) {
                $query->where('datetime_max_end', '<=', Carbon::now())
                    ->orWhereDoesntHave('bids', function (Builder $query) {
                        $query->where(
                            'created_at',
                            '>=',
                            DB::raw("NOW() - CAST(COALESCE(auctions.prolongation_step, 0)|| ' minutes' as Interval)")
                        );
                    });
            })->get();
    }

    /**
     * @return Collection|Auction[]
     */
    public function getNeededToStart(): Collection
    {
        return Auction::where('status', Auction::STATUS_WAIT)
            ->where('datetime_start', '<=', Carbon::now())
            ->get();
    }

    /**
     * @return Auction|null
     */
    public function getFinishedAuction(): ?Collection
    {
        return Auction::query()
            ->where('status', Auction::STATUS_FINISHED)
            ->get();
    }
}
