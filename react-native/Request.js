import axios from 'axios';

import RequestError from './RequestError';
import NetworkError from './NetworkError';
import Mock from './__mocks';
import { isMockEnabled } from '~/config';

class Request {
  constructor(url) {
    this.url = url;
    this.mock = new Mock();
  }

  createURL = path => `${this.url}/${path}`;

  request = async (url, options = {}) => {
    const requestOptions = { ...options };
    const requestURL = this.createURL(url);

    if (!requestOptions.headers) {
      requestOptions.headers = {};
    }

    const { headers, body } = requestOptions;

    if (body) {
      requestOptions.body = JSON.stringify(body);

      Object.assign(headers, {
        Accept: headers.Accept || 'application/json',
        'Content-Type': headers['Content-Type'] || 'application/json',
      });
    }

    let result;

    if (__DEV__ && isMockEnabled) {
      result = this.mock.getData(url);
    }

    if (!result) {
      try {
        result = await axios(requestURL, requestOptions);
      } catch (e) {
        return this.handleResponse(e);
      }
    }
    return this.handleResponse(result);
  }

  handleResponse = async response => {
    const contentType = response.headers && response.headers['Content-Type'];
    const isJSON = contentType && contentType.includes('json');

    if (response.status === 200) {
      return {
        data: response.data,
        headers: response.headers,
      };
    }

    const fields = {};

    const error = response;
    if (error.response) {
      const isErrorsExist = Boolean(Object.keys(error.response.data).length);
      if (!isErrorsExist) {
        if (error.response.status !== 200) {
          fields.request = `Ошибка сервера. ${error.response.status}: ${error.response.statusText}`;
        }
      } else {
        Object.entries(error.response.data.errors).forEach(([field, messages]) => {
          messages.forEach(msg => {
            fields[field] = msg;
          });
        });
      }
      throw new RequestError(
        `request error ${isJSON ? JSON.stringify(error) : error}`,
        error.response.status,
        isJSON && error,
        fields,
      );
    } else if (error.request) {
      throw new RequestError(
        `request error ${isJSON ? JSON.stringify(error) : error}`,
        error.request.status,
        isJSON && error,
        {
          request: 'Ошибка запроса',
        },
      );
    } else {
      throw new NetworkError(
        `network error ${error.message}`,
        500,
        isJSON && error,
        {
          request: 'Ошибка сети',
        },
      );
    }
  }
}

export default Request;
