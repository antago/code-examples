import React, { useState } from 'react';
import PropTypes from 'prop-types';
import {
  Text,
  Button,
  Input,
  Form,
  Item,
  Icon,
} from 'native-base';
import { View } from 'react-native';
import { connect } from 'react-redux';
import { Formik } from 'formik';
import * as yup from 'yup';
import { authErrors as authErrorsSelector } from '../../selectors/authSelector';
import { devUserPhone, devUserPassword } from '../../config';

import ServerError from '../ServerError/ServerError';

import styles from './AuthForm.styles';

import { formatPhone, getOnlyNumbers } from '../../global/utils';

import { login as loginAction, loginErrorHandler as loginErrorClear } from '../../ducks/auth';

function isDevUser(phone, password) {
  return (phone === devUserPhone && password === devUserPassword);
}

const AuthForm = ({
  login,
  navigation,
  authErrors,
  loginErrorHandler,
  isRecoverySucceed,
}) => {
  const [isSecure, changeSecure] = useState(true);

  return (
    <View>
      <Text style={styles.authTitle}>
        Авторизация
      </Text>
      <Formik
        initialValues={{ phone: '', password: '' }}
        onSubmit={values => login(values.phone, values.password)}
        validationSchema={yup.object().shape({
          phone: yup
            .string('Поле должно быть строкой')
            .required('Заполните поле'),
          password: yup
            .string()
            .max(6, 'Пароль должен содержать 6 символов')
            .min(6, 'Пароль должен содержать 6 символов')
            .required('Введите пароль'),
        })}
      >
        {({
          values,
          handleChange,
          errors,
          setFieldTouched,
          isValid,
        }) => (
          <>
            <Form>
              <View style={styles.authRow}>
                <Text style={styles.authRowLabel}>
                  Номер телефона
                </Text>
                <View style={styles.inputWrap}>
                  <Item style={styles.itemWrap}>
                    <Text style={{ fontSize: 18 }}>
                      +7
                    </Text>
                    <Input
                      value={formatPhone(values.phone)}
                      onChangeText={handleChange('phone')}
                      onBlur={() => setFieldTouched('phone')}
                      style={styles.authInput}
                      keyboardType="numeric"
                      maxLength={15}
                      autoFocus
                    />
                  </Item>
                </View>
                {errors.phone
                && (
                <Text style={styles.error}>
                  {errors.phone}
                </Text>
                )}
                {authErrors
                && (
                <Text style={styles.error}>
                  {authErrors.phone}
                </Text>
                )}
              </View>
              <Text style={styles.authRowLabel}>Пароль</Text>
              <View style={styles.inputWrap}>
                <Item style={styles.itemWrap}>
                  <Input
                    value={values.password}
                    onChangeText={handleChange('password')}
                    onBlur={() => setFieldTouched('password')}
                    secureTextEntry={isSecure}
                    style={styles.authInput}
                    keyboardType="numeric"
                    maxLength={6}
                  />
                  <Icon
                    name={!isSecure ? 'eye' : 'eye-off'}
                    type="Feather"
                    onPress={() => changeSecure(!isSecure)}
                  />
                </Item>
              </View>
              {errors.password
              && <Text style={styles.error}>{errors.password}</Text>}
              {authErrors
              && <Text style={styles.error}>{authErrors.password}</Text>}
              <Text
                style={styles.btnToRecovery}
                onPress={() => { navigation.navigate('Restore'); loginErrorHandler({ fields: {} }); }}
              >
                Получить пароль из смс
              </Text>
              <ServerError />
              <Button
                style={styles.authBtn}
                onPress={() => (isDevUser(`7${getOnlyNumbers(values.phone)}`, values.password)
                  ? navigation.navigate('ChooseServer')
                  : login(`7${getOnlyNumbers(values.phone)}`, values.password))}
                disabled={!isValid}
                block
                large
                primary
              >
                <Text>
                  ВОЙТИ
                </Text>
              </Button>
              {
                isRecoverySucceed
                && <Text style={styles.isRecoverySucceed}>Введите пароль полученный из СМС</Text>
              }
            </Form>
          </>
        )}
      </Formik>
    </View>
  );
};

AuthForm.propTypes = {
  login: PropTypes.func.isRequired,
  isRecoverySucceed: PropTypes.bool,
  loginErrorHandler: PropTypes.func.isRequired,
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired,
  }).isRequired,
  authErrors: PropTypes.shape({
    password: PropTypes.string,
    phone: PropTypes.string,
  }).isRequired,
};

AuthForm.defaultProps = {
  isRecoverySucceed: false,
};

const mapDispatchToProps = { login: loginAction, loginErrorHandler: loginErrorClear };

const mapStateToProps = state => ({
  authErrors: authErrorsSelector(state),
  isRecoverySucceed: state.auth.isRecoverySucceed,
});

export default connect(mapStateToProps, mapDispatchToProps)(AuthForm);
