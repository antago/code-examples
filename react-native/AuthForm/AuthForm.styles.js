import { StyleSheet } from 'react-native';
import { colors } from '~global/styles';

export default StyleSheet.create({
  authBtn: {
    fontSize: 14,
    marginTop: 20,
    shadowOffset: { height: 0, width: 0 },
    shadowOpacity: 0,
    elevation: 0,
  },

  authWrap: {
    paddingTop: 80,
    paddingHorizontal: 15,
    paddingBottom: 15,
    justifyContent: 'space-between',
    flex: 1,
  },

  authInput: {
    fontSize: 18,
    marginLeft: 1,
    paddingLeft: 12,
    borderWidth: 0,
  },

  itemWrap: {
    borderColor: 'transparent',
  },

  inputWrap: {
    borderWidth: 1,
    borderColor: colors.borderGrey,
  },

  authRowLabel: {
    fontSize: 14,
    color: colors.textGrey3,
    marginBottom: 5,
  },

  authRow: {
    marginBottom: 15,
  },

  authTitle: {
    fontSize: 24,
    textAlign: 'center',
    fontWeight: 'bold',
    marginBottom: 32,
  },

  btnToRecovery: {
    textAlign: 'right',
    fontSize: 18,
    color: colors.brandInfo,
    marginTop: 5,
  },

  error: {
    fontSize: 11,
    color: 'red',
  },

  isRecoverySucceed: {
    color: colors.textGrey3,
    textAlign: 'center',
    marginTop: 5,
  },
});
