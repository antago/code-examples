import { Record } from 'immutable';
import {
  all,
  call,
  put,
  takeLatest,
} from 'redux-saga/effects';
import { auth } from '../api';

import navigationService from '../services/navigatorService';
import { CLEAR_ERRORS } from './common';

// Constants
export const LOGIN_REQUEST = 'AUTH/LOGIN_REQUEST-NEXT';
export const LOGIN_RESPONSE = 'AUTH/LOGIN_RESPONSE';
export const AUTH_ERROR = 'AUTH/LOGIN_ERROR';
export const RECOVERY_REQUEST = 'RECOVERY/LOGIN_REQUEST-NEXT';
export const RECOVERY_RESPONSE = 'RECOVERY/LOGIN_RESPONSE';
export const LOGOUT_RESPONSE = 'AUTH/LOGOUT_RESPONSE';

// Records
export const ErrorsRecord = Record({
  request: '',
  phone: '',
  password: '',
}, 'AuthErrorsRecord');
export const ReducerRecord = Record({
  phone: '',
  password: '',
  token: '',
  isLoading: false,
  isRecoverySucceed: false,
  errors: new ErrorsRecord(),
}, 'AuthRootRecord');

// Reducer
export function reducer(state = new ReducerRecord(), action) {
  const { type, payload } = action;

  switch (type) {
    case LOGIN_RESPONSE: {
      return new ReducerRecord({
        phone: payload.phone,
        password: payload.password,
        token: payload.token,
      });
    }
    case AUTH_ERROR: {
      return state.set('errors', new ErrorsRecord(payload));
    }
    case RECOVERY_RESPONSE: {
      return new ReducerRecord({
        phone: payload.phone,
        isRecoverySucceed: true,
      });
    }
    case LOGOUT_RESPONSE: {
      return new ReducerRecord();
    }
    case CLEAR_ERRORS:
    case RECOVERY_REQUEST:
    case LOGIN_REQUEST: {
      return state.set('errors', new ErrorsRecord());
    }
    default: {
      return state;
    }
  }
}

// Action Creators
export function login(phone, password) {
  return {
    type: LOGIN_REQUEST,
    payload: {
      phone,
      password,
    },
  };
}

export function recovery({ phone }) {
  return {
    type: RECOVERY_REQUEST,
    payload: {
      phone,
    },
  };
}

export function loginResponse(payload) {
  return {
    type: LOGIN_RESPONSE,
    payload,
  };
}

export function recoveryResponse(payload) {
  return {
    type: RECOVERY_RESPONSE,
    payload,
  };
}

export function loginErrorHandler(error) {
  return {
    type: AUTH_ERROR,
    payload: error.fields,
  };
}

export function recoveryErrorHandler(error) {
  return {
    type: AUTH_ERROR,
    payload: error.fields,
  };
}

export function logout() {
  return {
    type: LOGOUT_RESPONSE,
  };
}

// Saga
function* loginSaga(action) {
  try {
    const { token } = yield call(auth.login, action.payload);
    yield put(loginResponse({
      ...action.payload,
      token,
    }));
  } catch (error) {
    yield put(loginErrorHandler(error));
  }
}

function* recoverySaga(action) {
  try {
    yield call(auth.recovery, action.payload);
    yield put(recoveryResponse({
      ...action.payload,
    }));
    navigationService.navigate('Auth');
  } catch (error) {
    yield put(recoveryErrorHandler(error));
  }
}

function* loginHandlerSaga() {
  yield takeLatest(LOGIN_REQUEST, loginSaga);
}

function* recoveryHandlerSaga() {
  yield takeLatest(RECOVERY_REQUEST, recoverySaga);
}

export function* saga() {
  yield all([
    loginHandlerSaga(),
    recoveryHandlerSaga(),
  ]);
}
