import Request from './request';

class Auth extends Request {
  login = async ({ phone, password }) => {
    const response = await this.request('login', {
      method: 'POST',
      data: {
        phone,
        password,
      },
    });

    return response.data;
  }

  recovery = async ({ phone }) => {
    const response = await this.request(`password/create?phone=${phone}`);

    return response.data;
  }

  logout = async () => {
    await this.request('logout', {
      method: 'POST',
    });
  }
}

export default Auth;
