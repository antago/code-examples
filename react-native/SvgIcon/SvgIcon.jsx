import React from 'react';
import PropTypes from 'prop-types';

// Icons
import TripsWave from './components/TripsWave';
import Union from './components/Union';

const icons = [
  {
    component: TripsWave,
    name: 'TripsWave',
  },
  {
    component: Union,
    name: 'Union',
  },
];

const SvgIcon = ({
  name,
  color,
  width,
  height,
  viewBox,
}) => {
  const Icon = icons.find(icon => icon.name === name);

  if (!Icon) { return null; }

  return (
    <Icon.component
      color={color}
      width={width}
      height={height}
      viewBox={viewBox}
    />
  );
};

SvgIcon.propTypes = {
  name: PropTypes.string.isRequired,
  color: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
  viewBox: PropTypes.string,
};

SvgIcon.defaultProps = {
  color: undefined,
  width: undefined,
  height: undefined,
  viewBox: undefined,
};

export default SvgIcon;
