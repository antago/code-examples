import React from 'react';
import PropTypes from 'prop-types';
import Svg, { Path, Circle } from 'react-native-svg';

const TripsWave = ({
  color,
  width,
  height,
  viewBox,
}) => (
  <Svg
    xmlns="http://www.w3.org/2000/svg"
    width={width}
    height={height}
    fill="none"
    viewBox={viewBox}
  >
    <Path
      stroke={color}
      strokeDasharray="4 4"
      strokeLinecap="round"
      d="M2.5 7.5L2.5 67.5"
    />
    <Circle cx="2.5" cy="2.5" r="2.5" fill={color} />
    <Circle cx="2.5" cy="66.5" r="2" fill="#fff" stroke={color} />
  </Svg>
);

TripsWave.propTypes = {
  color: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
  viewBox: PropTypes.string,
};

TripsWave.defaultProps = {
  color: '#9A3336',
  width: '5',
  height: '69',
  viewBox: '0 0 5 69',
};

export default TripsWave;
