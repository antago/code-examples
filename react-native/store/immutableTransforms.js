import immutableTransform from 'redux-persist-transform-immutable';
import { createTransform } from 'redux-persist';
import { Record } from 'immutable';

import {
  ErrorsRecord as AuthErrorsRecord,
  ReducerRecord as AuthRootRecord,
} from '../ducks/auth';
import {
  ReducerRecord as CommonRootRecord,
} from '../ducks/common';
import {
  ErrorsRecord as DriverErrorsRecord,
  DriverRecord,
  ReducerRecord as DriverRootRecord,
} from '../ducks/driver';
import {
  ErrorsRecord as TripsErrorsRecord,
  TripsRecord,
  ReducerRecord as TripsRootRecord,
} from '../ducks/trips';
import {
  ErrorsRecord as TripShowErrorsRecord,
  ReducerRecord as TripShowRootRecord,
  UploadDataRecord,
} from '../ducks/tripShow';

const clearLoading = createTransform(
  inboundState => {
    const isInstanceOfRecord = inboundState instanceof Record;
    const { isLoading } = inboundState;

    if (isLoading && isInstanceOfRecord) {
      return inboundState.set('isLoading', false);
    }

    return inboundState;
  },
);

const clearErrors = createTransform(
  inboundState => {
    const isInstanceOfRecord = inboundState instanceof Record;
    const { errors } = inboundState;

    if (errors && isInstanceOfRecord && errors instanceof Record) {
      return inboundState.set('errors', inboundState.errors.clear());
    }

    return inboundState;
  },
);

export default [
  clearLoading,
  clearErrors,
  immutableTransform({
    records: [
      AuthErrorsRecord,
      AuthRootRecord,
      DriverErrorsRecord,
      DriverRecord,
      DriverRootRecord,
      TripsErrorsRecord,
      TripsRecord,
      TripsRootRecord,
      TripShowErrorsRecord,
      TripShowRootRecord,
      UploadDataRecord,
      CommonRootRecord,
    ],
  }),
];
