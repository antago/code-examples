import {
  applyMiddleware,
  compose,
  createStore,
} from 'redux';
import AsyncStorage from '@react-native-community/async-storage';
import {
  persistReducer,
  persistStore,
} from 'redux-persist';
import semver from 'semver';
import DeviceInfo from 'react-native-device-info';
import axios from 'axios';
import { isLogged } from '../config';

import rootSaga from './rootSaga';

import storeService from '../services/storeService';

import reduxSagaInit, { sagaMiddleware } from './init/sagaInit';
import networkInit from './init/networkInit';
import loggerInit from './init/loggerInit';

import createMainReducer from './createRootReducer';
import transforms from './immutableTransforms';
import { LOGOUT_RESPONSE } from '~ducks/auth';

export default (
  callback = () => {},
) => {
  const middleware = [
    ...networkInit.middleware,
    ...reduxSagaInit.middleware,
    ...loggerInit.middleware,
  ];

  const combinedReducer = createMainReducer();
  const persistConfig = {
    key: 'root',
    storage: AsyncStorage,
    transforms,
    blacklist: ['common', 'nav'],
    // eslint-disable-next-line no-unused-vars
    migrate: async state => {
      const currentVer = await DeviceInfo.getVersion();
      const oldVersion = await AsyncStorage.getItem('version');

      await AsyncStorage.setItem('version', currentVer);

      if (state && (!oldVersion || !semver.eq(currentVer, oldVersion))) {
        return Promise.resolve({
          ...createStore(combinedReducer).getState(),
        });
      }

      return Promise.resolve(state);
    },
  };

  const mainReducer = persistReducer(persistConfig, combinedReducer);
  const enhancer = compose(applyMiddleware(...middleware));
  const store = createStore(mainReducer, undefined, enhancer);
  const persistor = persistStore(store, null, callback);

  sagaMiddleware.run(rootSaga);

  axios.interceptors.request.use(config => {
    const { token } = store.getState().auth;

    // eslint-disable-next-line no-param-reassign
    config.headers = {
      ...config.headers,
      Authorization: `Bearer ${token}`,
    };

    if (isLogged) {
      console.log('request', config);
    }
    return config;
  }, error => {
    if (isLogged) {
      console.log('request error', JSON.stringify(error));
    }
    return Promise.reject(error);
  });

  axios.interceptors.response.use(response => {
    if (isLogged) {
      console.log('response', response);
    }

    return response;
  }, error => {
    if (error.response.status === 401) {
      store.dispatch({
        type: LOGOUT_RESPONSE,
      });
    }
    if (isLogged) {
      console.log('response error', JSON.stringify(error));
    }
    return Promise.reject(error);
  });

  storeService.store = store;

  return {
    store,
    persistor,
  };
};
