import { combineReducers } from 'redux';
import { reducer as network } from 'react-native-offline';
import { reducer as auth } from '~ducks/auth';
import { reducer as driver } from '~ducks/driver';
import { reducer as trips } from '~ducks/trips';
import { reducer as tripShow } from '~ducks/tripShow';
import { reducer as common } from '~ducks/common';

export default () => {
  const reducers = {
    network,
    auth,
    driver,
    trips,
    tripShow,
    common,
  };

  return combineReducers(reducers);
};
