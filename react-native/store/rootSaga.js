import { all, fork } from 'redux-saga/effects';
import { networkSaga } from 'react-native-offline';
import { saga as authSaga } from '../ducks/auth';
import { saga as driverSaga } from '../ducks/driver';
import { saga as tripsSaga } from '../ducks/trips';
import { saga as tripShowSaga } from '../ducks/tripShow';

export default function* rootSaga() {
  yield all([
    fork(networkSaga, { pingInterval: 10000 }),
    fork(authSaga),
    fork(driverSaga),
    fork(tripsSaga),
    fork(tripShowSaga),
  ]);
}
