import { createNetworkMiddleware } from 'react-native-offline';

const networkMiddleware = createNetworkMiddleware({
  regexActionType: /.*INTERCEPTED/,
});

export default {
  middleware: [networkMiddleware],
};
