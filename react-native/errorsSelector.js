import { createSelector } from 'reselect';

const authSelector = state => state.auth;
const tripsSelector = state => state.trips;
const tripShowSelector = state => state.tripShow;

export const authRequestError = createSelector(
  authSelector,
  auth => auth.errors.request,
);

export const tripsRequestError = createSelector(
  tripsSelector,
  trips => trips.errors.request,
);

export const tripShowRequestError = createSelector(
  tripShowSelector,
  tripShow => tripShow.errors.request,
);

export const requestError = createSelector(
  [authRequestError, tripsRequestError, tripShowRequestError],
  (authRequestError, tripsRequestError, tripShowRequestError) => {
    return authRequestError || tripsRequestError || tripShowRequestError;
  },
);
