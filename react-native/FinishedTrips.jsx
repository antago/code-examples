import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { ReducerRecord as TripRootRecord, trips as tripsAction } from '../ducks/trips';
import ContentWrapper from '~features/ContentWrapper';
import ViewMore from '~components/ViewMore';
import TripList from '~components/TripList';
import { isPaginable as isPaginableSelector } from '~selectors/tripsSelector';
import ServerError from '../components/ServerError';

const FinishedTrips = ({
  trips,
  loadTrips,
  isPaginable,
  navigation,
}) => {
  useEffect(() => {
    loadTrips(true);
  }, []);

  return (
    <ContentWrapper
      onRefresh={() => loadTrips(true, true)}
      isRefreshing={trips.isLoading}
    >
      <ServerError />
      <TripList
        trips={trips.completedTrips}
        loading={trips.isLoading}
        currentPage={trips.currentPage}
        navigation={navigation}
      />
      {isPaginable && <ViewMore onPress={() => loadTrips(true)} />}
    </ContentWrapper>
  );
};

FinishedTrips.propTypes = {
  trips: PropTypes.instanceOf(TripRootRecord).isRequired,
  loadTrips: PropTypes.func.isRequired,
  isPaginable: PropTypes.bool.isRequired,
  navigation: PropTypes.shape().isRequired,
};

const mapDispatchToProps = { loadTrips: tripsAction };

const mapStateToProps = state => ({
  trips: state.trips,
  isPaginable: isPaginableSelector(state),
});

export default connect(mapStateToProps, mapDispatchToProps)(FinishedTrips);
