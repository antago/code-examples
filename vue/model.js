import { clone } from 'chober';
import { objectify } from './utils';

const objectify = (object, [key, value]) => ({
  ...object,
  [key]: value,
});

export default model => (data = {}) => clone(Object.entries(model)
  .map(([key, value]) => [
    key,
    data[key] !== undefined ? data[key] : value,
  ])
  .reduce(objectify, {}));
