import model from '@global/model';

export default model({
  id: null,
  status: null,
  driver: null,
});
