import model from '@global/model';

export default model({
  config: {},
  route: {},
  isShownToggleButton: true,
  isShownMap: false,
  isMapLoaded: false,
});
