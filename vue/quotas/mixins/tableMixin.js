// Vuex
import { mapState } from 'vuex';

// Utils
import { objectify, groupBy } from '@global/utils';

export default {
  data: () => ({
    headerHeight: 0,
    stickyOffset: { top: 0 },
    stickyOffsetPeriods: { top: 0 },
  }),

  computed: {
    ...mapState([
      'loadTime',
    ]),

    gutterWidth() {
      const canUseCSSVars = window.CSS && window.CSS.supports('color', 'var(--some-var)');

      if (!canUseCSSVars) return 24; // must coincide with $gutter-width

      return this.isLargeDesktop ? 40 : 24;
    },

    gutterCompensationWidth() {
      return -2 * this.gutterWidth / this.dateValues.length;
    },
  },

  watch: {
    headerHeight() {
      this.setStickyOffset();
      this.setStickyOffsetPeriods();
    },

    loadTime() {
      this.setStickyOffset();
      this.setStickyOffsetPeriods();
    },
  },

  mounted() {
    this.setHeaderHeight();
    window.addEventListener('resize', this.setHeaderHeight);
  },

  methods: {
    getColumnWidth(index) {
      const columnCount = this.dateValues.length;
      const isFirstColumn = index === 0;
      const columnWidthDeviation = isFirstColumn
        ? this.gutterCompensationWidth + this.gutterWidth
        : this.gutterCompensationWidth;

      return `calc(${100 / columnCount}% + ${columnWidthDeviation}px)`;
    },

    getPeriodedItems(items) {
      return {
        ...this.dateValues
          .map(period => [period, []])
          .reduce(objectify, {}),
        ...groupBy(items, 'date'),
      };
    },

    setStickyOffset() {
      const tablePeriods = document.querySelector('.j-table-periods');
      const periodHeight = tablePeriods === null ? 34 : tablePeriods.clientHeight;

      this.stickyOffset = { top: periodHeight + this.headerHeight };
    },

    setStickyOffsetPeriods() {
      this.stickyOffsetPeriods = { top: this.headerHeight };
    },

    setHeaderHeight() {
      const header = document.querySelector('.j-mobile-header');

      this.headerHeight = (header && header.clientHeight) || 0;
    },
  },
};
