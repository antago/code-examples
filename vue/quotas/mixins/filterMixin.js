// Vuex
import {
  mapState,
  mapMutations,
} from 'vuex';

// Mixins
import UIMixin from '@mixins/UIMixin';

export default {
  mixins: [UIMixin],

  data: () => ({
    timeFromLoad: '',
  }),

  computed: {
    ...mapState([
      'isPreloaderShown',
      'loadTime',
      'isTabletFiltersShown',
    ]),
  },

  mounted() {
    this.timeFromLoad = this.fromNow(this.loadTime);

    setInterval(() => {
      this.timeFromLoad = this.fromNow(this.loadTime);
    }, 1000);
  },

  methods: {
    ...mapMutations([
      'updateState',
    ]),

    closeForm() {
      this.updateState({ isTabletFiltersShown: false });
    },
  },
};
