import moment from 'moment';

export default {
  isPreloaderShown: false,
  isQuotaUpdating: false,
  hasError: false,
  list: {},
  loadTime: '',
  quotas: {},
  isOpenForm: false,
  selectedDate: moment().format('YYYY-MM-DD'),
  createdQuotas: [],
  selectedDateToCreate: null,
  selectedFarm: null,
  selectedFarmId: null,
  selectedQuotaId: null,
  quota: {},
  updatingFarmId: null,
  errorsQuota: null,
  isOpenCommentPopup: false,
  factoryTotalCapacity: 0,
  hasWarning: false,
};
