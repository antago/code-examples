// Libs
import Vue from 'vue';
import VueResource from 'vue-resource';
import { getQuery } from 'chober';
import moment from 'moment';

// Utils
import { throwResponse } from '@global/utils';

Vue.use(VueResource);

export default {
  GET_QUOTAS({ commit }) {
    const params = getQuery();
    const { selectedDate } = params;

    const incomingDateFormat = selectedDate === undefined ? '' : 'DD.MM.Y';

    params.firstDate = moment(selectedDate, incomingDateFormat)
      .subtract(1, 'days')
      .format('DD.MM.Y');
    params.lastDate = moment(selectedDate, incomingDateFormat)
      .add(3, 'days')
      .format('DD.MM.Y');

    return Vue.http.get(laroute.route('quotas.list.ajax'), { params })
      .then(({ body }) => {
        commit('updateState', {
          list: body,
          factoryTotalCapacity: body.factoryTotalCapacity,
        });
      })
      .catch(throwResponse)
      .finally(() => {
        commit('updateState', {
          isQuotaUpdating: false,
        });
      });
  },
};
