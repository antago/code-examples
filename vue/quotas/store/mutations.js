export default {
  updateState(state, data) {
    Object.assign(state, data);
  },

  updateFarm(state, { farmIndex, farmData }) {
    state.list.farms[farmIndex] = farmData;
  },

  createQuota(state, data) {
    const existedQuotaIndex = state.createdQuotas.findIndex(quota => (
      quota.farmId === data.farmId
        && quota.date === data.date
    ));

    if (existedQuotaIndex !== -1) {
      state.createdQuotas[existedQuotaIndex] = data;
    } else {
      state.createdQuotas.push(data);
    }
  },
};
