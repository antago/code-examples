export default {
  quotas: state => state.list.quotas,
  farms: state => state.list.farms || [],
  role: state => state.list.role || [],
  totalWeight: state => state.list.totalWeight || {},
  farmOptions: state => state.list.farms && state.list.farms.map(farm => ({
    text: farm.name,
    value: farm.id,
  })),
};
