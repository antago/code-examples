import model from '@global/model';

export default model({
  capacity: 0,
  nightCapacity: 0,
  farmId: null,
  date: '',
  id: null,
  shareBetweenAll: 0,
  withoutShare: 1,
  selectedOption: 2,
  shareWithFarm: null,
  acceptanceTimeFrom: '',
  acceptanceTimeTo: '',
  farmWithoutTrash: {},
  comment: '',
  logs: [],
});
