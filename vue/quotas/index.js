// Libs
import Vue from 'vue';

// Scripts
import '@global/bootstrap';

// Store
import store from './store';

// Components
import TheQuotas from './TheQuotas';

const app = new Vue({
  store,
  render: h => h(TheQuotas),
});

app.$mount('#quotas');
